import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { SingleSelect } from 'react-select-material-ui';
import Link from '@material-ui/core/Link';

const styles = theme => ({
    root: {
        fontFamily: 'Roboto, sans-serif',
        marginRight: theme.spacing.unit * 3,
        '& > div > div': {
            marginBottom: theme.spacing.unit,
            padding: '5px 10px 5px 15px',
            border: '1px solid #bbb',
            borderRadius: '5px'
        },
        '& > div > div > div > div': {
            borderBottom: 'none',
            background: '#fff !important'
        },
    },
    filterComponent: {
        display: 'flex',
        flexDirection: 'column'
    },
    addFilter: {
        width: 257,
        border: '1px dashed #ccc',
        borderRadius: '5px',
        marginTop: '-31px',
        '& button': {
            color: '#444',
            padding: theme.spacing.unit + 4,
            display: 'block',
            textDecoration: 'none',
            width: '100%'
        }
    },
    removeFilter: {
        color: '#18b5d7',
        textAlign: 'right',
        textDecoration: 'underline'
    }
});

class FilterDropdown extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isFilterAvailable: props.isFilterAvailable
        }
    }

    handleAddFilter = () => {
        this.setState(
            { isFilterAvailable: true },
            this.props.documentFilter(this.props.filterName)
        );
    }

    handleRemoveFilter = () => {
        this.setState(
            { isFilterAvailable: false },
            this.props.undocumentFilter(this.props.filterName)
        );
    }

    showFilterComponent = () => {
        const {
            classes,
            options,
            selectedOption,
            handleChange,
            placeholder
        } = this.props;

        return (
            <div className={classes.filterComponent}>
                <SingleSelect 
                    style={{width: 230}}
                    value={selectedOption}
                    options={options}
                    onChange={handleChange}
                    placeholder={placeholder}
                    SelectProps={{
                        isClearable: true
                    }}
                />
                <Link
                    className={classes.removeFilter}
                    component="button"
                    variant="body2"
                    onClick={this.handleRemoveFilter}
                >
                    Remove filter
                </Link>
            </div>
        );
    }

    showAddFilter = () => {
        const { classes } = this.props;

        return (
            <div className={classes.addFilter}>
                <Link
                    component="button"
                    variant="body2"
                    onClick={this.handleAddFilter}
                >
                    { this.props.addFilterLabel }
                </Link>
            </div>
        ); 
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                {
                    this.state.isFilterAvailable ?
                    this.showFilterComponent() :
                    this.showAddFilter()
                }
            </div>
        );
    }
}

FilterDropdown.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(FilterDropdown);