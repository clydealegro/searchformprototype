import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FilterDropdown from './FilterDropdown';
import Button from '@material-ui/core/Button';
import SearchStatus from './SearchStatus';
import Snackbar from '@material-ui/core/Snackbar';

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 4,
    },
    form: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: '100px'
    },
    searchBtn: {
        backgroundColor: '#dd625a',
        marginTop: '-30px',
        padding: '12px 24px',
        '&:hover': {
            backgroundColor: '#c74138'
        }
    }
});

class App extends Component {
    state = {
        country: null,
        subject: null,
        term: null,
        filters: ['country'],
        showSnackbar: false
    }

    handleChangeCountry = selectedOption => {
        this.setState({
            country: selectedOption
        });
    }

    handleChangeSubject = selectedOption => {
        this.setState({
            subject: selectedOption
        });
    }

    handleChangeTerm = selectedOption => {
        this.setState({
            term: selectedOption
        });
    }

    documentFilter = filterName => {
        const newFilter = [...this.state.filters, `${filterName}`];

        this.setState({
            filters: newFilter
        })
    }

    undocumentFilter = filterName => {
        const newFilter = this.state.filters;

        newFilter.splice(newFilter.indexOf(`${filterName}`), 1);

        this.setState({
            filters: newFilter,
            [`${filterName}`]: null
        });
    }

    handleClickSearch = () => {
        this.setState({
            showSnackbar: true
        });
    }

    handleCloseSnackbar = () => {
        this.setState({
            showSnackbar: false
        });
    }

    render() {
        const { classes } = this.props;
        const { selectedOption, filters, country, subject, term } = this.state;
        const countryOptions = ['Australia','China','France','Spain','Japan','Korea','Indonesia'];
        const subjectOptions = ['Accounting', 'Nursing', 'Veterinary', 'Linguistic', 'Marketing'];
        const termOptions = ['Winter', 'Spring', 'Summer', 'Fall'];

        return (
            <div className={classes.root}>
                <SearchStatus 
                    country={country}
                    subject={subject}
                    term={term}
                />
                <div className={classes.form}>
                    <FilterDropdown
                        filterName="country"
                        handleChange={this.handleChangeCountry} 
                        selectedOption={selectedOption}
                        options={countryOptions}
                        placeholder="Choose a Country"
                        addFilterLabel="+ ADD A COUNTRY FILTER"
                        isFilterAvailable
                        documentFilter={this.documentFilter}
                        undocumentFilter={this.undocumentFilter}
                    />
                    <FilterDropdown
                        filterName="term"
                        handleChange={this.handleChangeTerm} 
                        selectedOption={selectedOption}
                        options={termOptions}
                        placeholder="Choose a Term"
                        addFilterLabel="+ ADD A TERM FILTER"
                        documentFilter={this.documentFilter}
                        undocumentFilter={this.undocumentFilter}
                    />
                    <FilterDropdown
                        filterName="subject"
                        handleChange={this.handleChangeSubject} 
                        selectedOption={selectedOption}
                        options={subjectOptions}
                        placeholder="Choose a Subject"
                        addFilterLabel="+ ADD A SUBJECT FILTER"
                        documentFilter={this.documentFilter}
                        undocumentFilter={this.undocumentFilter}
                    />
                    <Button 
                        className={classes.searchBtn}
                        variant="contained" 
                        color="primary"
                        disabled={!(filters.length > 0)}
                        size="large"
                        onClick={this.handleClickSearch}
                    >
                        Search Programs
                    </Button>
                    <Snackbar
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                        open={this.state.showSnackbar}
                        onClose={this.handleCloseSnackbar}
                        message={<span>Searching Programs ...</span>}
                        autoHideDuration={2000}
                    />
                </div>
            </div>
        );
    }
}

App.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(App);