import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    root: {
        marginBottom: theme.spacing.unit * 2,
        textAlign: 'center',
        '& span': {
            fontWeight: 'bold',
            textDecoration: 'underline'
        }
    }
});

class SearchStatus extends Component {
    render() {
        const { classes, country, term, subject } = this.props;

        return (
            <div className={classes.root}>
                {
                    (country || term || subject) ?
                    <Typography>
                        You are searching Study Abroad 
                        { term && ' '}<span>{ term && `${term}`}</span>
                        { subject && ' ' }<span>{ subject && `${subject}`}</span>
                        {` `}programs
                        { country && ` in ` }<span>{ country && `${country}` }</span>. 
                        Click the button to continue.
                    </Typography> :
                    <Typography>Choose a country or add filters to refine your search. Click button to continue.</Typography>
                }
            </div>
        );
    }
}

SearchStatus.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(SearchStatus);